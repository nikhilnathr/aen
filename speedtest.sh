#!/bin/bash

s=0.0
num=25
echo "Compiling release build"
make release

test_size=127622380
echo "Making random $test_size byte file (/tmp/message.txt)"
cat /dev/urandom | head -c 127622680 > /tmp/message.txt

echo "Start test with $num cycles"
for number in `seq 0 $(( $num - 1 ))`;
do
    # (( $number % 25 == 0 )) && (( $number != 0 )) && echo "Ran $number times..."
    # exec_time=$(cat /dev/urandom | head -c 127622680 > message.txt && /usr/bin/time -v ./bin/main message.txt 2>&1 | grep Elapsed | cut -d':' -f6)
    exec_time=$(/usr/bin/time -v ./bin/main /tmp/message.txt 2>&1 | grep Elapsed | cut -d':' -f6)
    echo $exec_time
    s=$(echo "$s + $exec_time" | bc)
done

echo "Total: $s | Iterations: $num"
echo -n "Average: "
echo "$s / $num" | bc -l