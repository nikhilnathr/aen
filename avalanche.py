#!/usr/bin/python3
from subprocess import Popen, PIPE, STDOUT
import random
import sys

def hamming(a, b):
    a = int(a, 16)
    b = int(b, 16)
    c = format(a ^ b, "b")
    one_count = list(c).count("1")
    return one_count

a=0xf023d85b818e2534db1d2b2a140e8b68d0d3f942305502bbf80da3ac71203b5015d4a0aea6a74e088ad3ca34edb9c045f28d5593b7700a444c3dff78d7b38b0a57e4afbe1943583efbe91281d89bcad710b49bfa78a8f994df7c1d1d0dce96dee0b51115c23a0dff9a13e7405382a19b797be6d9d1ad42660bb15be1db06207555c6dcf763c064ca5c9bd6b2d1f6836f4057327a110fac12600166828459f8103e0656d7f992a89f3faa39487e9d3d6fc1968d77d7326f45d4d1c1754a10311c8660ce1e841433114201f9bd462b464cc9cd466352ec48d129dd5e569bcef6366380e94aa5ba59e48e0f9bf414bac0d4bad4b186a48d
ham = []
runs = 10000

print("Initial vector hash %d bits, running for %d times" % (len(list(format(a, "b"))), runs))

for i in range(runs):
    b = a
    b = list(format(b, "b"))
    random_bit = random.randrange(len(b))
    b[random_bit] = str(int(not int(b[random_bit])))
    b = int("".join(b), 2)
    p = Popen([sys.argv[1]], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    out = p.communicate(input=bytes(hex(a), 'utf-8'))
    a_out = out[0].splitlines()[-1].decode()
    p = Popen([sys.argv[1]], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    out = p.communicate(input=bytes(hex(b), 'utf-8'))
    b_out = out[0].splitlines()[-1].decode()

    ham_now = hamming(a_out, b_out)
    if (ham_now == 0):
        print(a)
        print(b)
        print(random_bit)
        print(hamming(a, b))
        print()

    ham.append(ham_now)

s = sum(ham)
a = s / runs

print("Hamming range: %d, %d" % (min(ham), max(ham)))
print("Change average: %5.2f" % (a))
print("Change probability: %5.2f" % (a*100/256))
