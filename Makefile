CC = clang
CFLAGS_DEBUG = -Wall -g3
CFLAGS = -Wall -O3

HASHING_DIR = hashing

all: release

debug:
	mkdir -p build/debug/hashing build/debug/helpers bin

	$(CC) $(CFLAGS_DEBUG) -c src/hashing/main.c -o build/debug/hashing/main.o
	$(CC) $(CFLAGS_DEBUG) -c src/hashing/modified_cash128.c -o build/debug/hashing/modified_cash128.o
	$(CC) $(CFLAGS_DEBUG) -c src/hashing/sponge.c -o build/debug/hashing/sponge.o
	$(CC) $(CFLAGS_DEBUG) -c src/helpers/helpers.c -o build/debug/helpers/helpers.o

	$(CC) $(CFLAGS_DEBUG) -o bin/main.debug build/debug/hashing/main.o build/debug/hashing/modified_cash128.o build/debug/hashing/sponge.o build/debug/helpers/helpers.o

release:
	mkdir -p build/release/hashing build/release/helpers bin

	$(CC) $(CFLAGS) -funroll-loops -c src/hashing/main.c -o build/release/hashing/main.o
	$(CC) $(CFLAGS) -funroll-loops -c src/hashing/modified_cash128.c -o build/release/hashing/modified_cash128.o
	$(CC) $(CFLAGS) -funroll-loops -c src/hashing/sponge.c -o build/release/hashing/sponge.o
	$(CC) $(CFLAGS) -funroll-loops -c src/helpers/helpers.c -o build/release/helpers/helpers.o

	$(CC) $(CFLAGS) -funroll-loops -o bin/main build/release/hashing/main.o build/release/hashing/modified_cash128.o build/release/hashing/sponge.o build/release/helpers/helpers.o

release-gcc: export CC = gcc
release-gcc: release

release-cpp: export CC = clang++
release-cpp: release

release-g++: export CC = g++
release-g++: release

clean:
	rm -rf build bin
