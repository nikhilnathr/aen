#include <stdint.h>
#include <string.h>
#include <byteswap.h>
#include "constants.h"
#define KAPPA 0b00000110

// All CAs used are null bounded
void swap_bits(void *state, int a, int b) {
    char *u = (char *)(state + (a / 8));
    char *v = (char *)(state + (b / 8));

    char x = (*u >> (7 - (a % 8))) & 1;
    char y = (*v >> (7 - (b % 8))) & 1;

    *u ^= ((x ^ y) << (7 - (a % 8)));
    *v ^= ((x ^ y) << (7 - (b % 8)));
}

void cash128_permute(void *state) {
    // for (int i = 0; i < RATE; i++) {
    //     // if (i%2) {
    //         swap_bits(state, i, RATE + ((i * 11) % (CAPACITY - 1)));
    //     // }
    //     // printf("swap_bits(state, %d, %d);\n", i, RATE + ((i * 11) % (CAPACITY - 1)));
    // };
    swap_bits(state, 0, 248);
swap_bits(state, 1, 259);
swap_bits(state, 2, 270);
swap_bits(state, 3, 281);
swap_bits(state, 4, 292);
swap_bits(state, 5, 303);
swap_bits(state, 6, 314);
swap_bits(state, 7, 325);
swap_bits(state, 8, 336);
swap_bits(state, 9, 347);
swap_bits(state, 10, 358);
swap_bits(state, 11, 369);
swap_bits(state, 12, 380);
swap_bits(state, 13, 391);
swap_bits(state, 14, 402);
swap_bits(state, 15, 413);
swap_bits(state, 16, 424);
swap_bits(state, 17, 435);
swap_bits(state, 18, 446);
swap_bits(state, 19, 457);
swap_bits(state, 20, 468);
swap_bits(state, 21, 479);
swap_bits(state, 22, 490);
swap_bits(state, 23, 501);
swap_bits(state, 24, 249);
swap_bits(state, 25, 260);
swap_bits(state, 26, 271);
swap_bits(state, 27, 282);
swap_bits(state, 28, 293);
swap_bits(state, 29, 304);
swap_bits(state, 30, 315);
swap_bits(state, 31, 326);
swap_bits(state, 32, 337);
swap_bits(state, 33, 348);
swap_bits(state, 34, 359);
swap_bits(state, 35, 370);
swap_bits(state, 36, 381);
swap_bits(state, 37, 392);
swap_bits(state, 38, 403);
swap_bits(state, 39, 414);
swap_bits(state, 40, 425);
swap_bits(state, 41, 436);
swap_bits(state, 42, 447);
swap_bits(state, 43, 458);
swap_bits(state, 44, 469);
swap_bits(state, 45, 480);
swap_bits(state, 46, 491);
swap_bits(state, 47, 502);
swap_bits(state, 48, 250);
swap_bits(state, 49, 261);
swap_bits(state, 50, 272);
swap_bits(state, 51, 283);
swap_bits(state, 52, 294);
swap_bits(state, 53, 305);
swap_bits(state, 54, 316);
swap_bits(state, 55, 327);
swap_bits(state, 56, 338);
swap_bits(state, 57, 349);
swap_bits(state, 58, 360);
swap_bits(state, 59, 371);
swap_bits(state, 60, 382);
swap_bits(state, 61, 393);
swap_bits(state, 62, 404);
swap_bits(state, 63, 415);
swap_bits(state, 64, 426);
swap_bits(state, 65, 437);
swap_bits(state, 66, 448);
swap_bits(state, 67, 459);
swap_bits(state, 68, 470);
swap_bits(state, 69, 481);
swap_bits(state, 70, 492);
swap_bits(state, 71, 503);
swap_bits(state, 72, 251);
swap_bits(state, 73, 262);
swap_bits(state, 74, 273);
swap_bits(state, 75, 284);
swap_bits(state, 76, 295);
swap_bits(state, 77, 306);
swap_bits(state, 78, 317);
swap_bits(state, 79, 328);
swap_bits(state, 80, 339);
swap_bits(state, 81, 350);
swap_bits(state, 82, 361);
swap_bits(state, 83, 372);
swap_bits(state, 84, 383);
swap_bits(state, 85, 394);
swap_bits(state, 86, 405);
swap_bits(state, 87, 416);
swap_bits(state, 88, 427);
swap_bits(state, 89, 438);
swap_bits(state, 90, 449);
swap_bits(state, 91, 460);
swap_bits(state, 92, 471);
swap_bits(state, 93, 482);
swap_bits(state, 94, 493);
swap_bits(state, 95, 504);
swap_bits(state, 96, 252);
swap_bits(state, 97, 263);
swap_bits(state, 98, 274);
swap_bits(state, 99, 285);
swap_bits(state, 100, 296);
swap_bits(state, 101, 307);
swap_bits(state, 102, 318);
swap_bits(state, 103, 329);
swap_bits(state, 104, 340);
swap_bits(state, 105, 351);
swap_bits(state, 106, 362);
swap_bits(state, 107, 373);
swap_bits(state, 108, 384);
swap_bits(state, 109, 395);
swap_bits(state, 110, 406);
swap_bits(state, 111, 417);
swap_bits(state, 112, 428);
swap_bits(state, 113, 439);
swap_bits(state, 114, 450);
swap_bits(state, 115, 461);
swap_bits(state, 116, 472);
swap_bits(state, 117, 483);
swap_bits(state, 118, 494);
swap_bits(state, 119, 505);
swap_bits(state, 120, 253);
swap_bits(state, 121, 264);
swap_bits(state, 122, 275);
swap_bits(state, 123, 286);
swap_bits(state, 124, 297);
swap_bits(state, 125, 308);
swap_bits(state, 126, 319);
swap_bits(state, 127, 330);
swap_bits(state, 128, 341);
swap_bits(state, 129, 352);
swap_bits(state, 130, 363);
swap_bits(state, 131, 374);
swap_bits(state, 132, 385);
swap_bits(state, 133, 396);
swap_bits(state, 134, 407);
swap_bits(state, 135, 418);
swap_bits(state, 136, 429);
swap_bits(state, 137, 440);
swap_bits(state, 138, 451);
swap_bits(state, 139, 462);
swap_bits(state, 140, 473);
swap_bits(state, 141, 484);
swap_bits(state, 142, 495);
swap_bits(state, 143, 506);
swap_bits(state, 144, 254);
swap_bits(state, 145, 265);
swap_bits(state, 146, 276);
swap_bits(state, 147, 287);
swap_bits(state, 148, 298);
swap_bits(state, 149, 309);
swap_bits(state, 150, 320);
swap_bits(state, 151, 331);
swap_bits(state, 152, 342);
swap_bits(state, 153, 353);
swap_bits(state, 154, 364);
swap_bits(state, 155, 375);
swap_bits(state, 156, 386);
swap_bits(state, 157, 397);
swap_bits(state, 158, 408);
swap_bits(state, 159, 419);
swap_bits(state, 160, 430);
swap_bits(state, 161, 441);
swap_bits(state, 162, 452);
swap_bits(state, 163, 463);
swap_bits(state, 164, 474);
swap_bits(state, 165, 485);
swap_bits(state, 166, 496);
swap_bits(state, 167, 507);
swap_bits(state, 168, 255);
swap_bits(state, 169, 266);
swap_bits(state, 170, 277);
swap_bits(state, 171, 288);
swap_bits(state, 172, 299);
swap_bits(state, 173, 310);
swap_bits(state, 174, 321);
swap_bits(state, 175, 332);
swap_bits(state, 176, 343);
swap_bits(state, 177, 354);
swap_bits(state, 178, 365);
swap_bits(state, 179, 376);
swap_bits(state, 180, 387);
swap_bits(state, 181, 398);
swap_bits(state, 182, 409);
swap_bits(state, 183, 420);
swap_bits(state, 184, 431);
swap_bits(state, 185, 442);
swap_bits(state, 186, 453);
swap_bits(state, 187, 464);
swap_bits(state, 188, 475);
swap_bits(state, 189, 486);
swap_bits(state, 190, 497);
swap_bits(state, 191, 508);
swap_bits(state, 192, 256);
swap_bits(state, 193, 267);
swap_bits(state, 194, 278);
swap_bits(state, 195, 289);
swap_bits(state, 196, 300);
swap_bits(state, 197, 311);
swap_bits(state, 198, 322);
swap_bits(state, 199, 333);
swap_bits(state, 200, 344);
swap_bits(state, 201, 355);
swap_bits(state, 202, 366);
swap_bits(state, 203, 377);
swap_bits(state, 204, 388);
swap_bits(state, 205, 399);
swap_bits(state, 206, 410);
swap_bits(state, 207, 421);
swap_bits(state, 208, 432);
swap_bits(state, 209, 443);
swap_bits(state, 210, 454);
swap_bits(state, 211, 465);
swap_bits(state, 212, 476);
swap_bits(state, 213, 487);
swap_bits(state, 214, 498);
swap_bits(state, 215, 509);
swap_bits(state, 216, 257);
swap_bits(state, 217, 268);
swap_bits(state, 218, 279);
swap_bits(state, 219, 290);
swap_bits(state, 220, 301);
swap_bits(state, 221, 312);
swap_bits(state, 222, 323);
swap_bits(state, 223, 334);
swap_bits(state, 224, 345);
swap_bits(state, 225, 356);
swap_bits(state, 226, 367);
swap_bits(state, 227, 378);
swap_bits(state, 228, 389);
swap_bits(state, 229, 400);
swap_bits(state, 230, 411);
swap_bits(state, 231, 422);
swap_bits(state, 232, 433);
swap_bits(state, 233, 444);
swap_bits(state, 234, 455);
swap_bits(state, 235, 466);
swap_bits(state, 236, 477);
swap_bits(state, 237, 488);
swap_bits(state, 238, 499);
swap_bits(state, 239, 510);
swap_bits(state, 240, 258);
swap_bits(state, 241, 269);
swap_bits(state, 242, 280);
swap_bits(state, 243, 291);
swap_bits(state, 244, 302);
swap_bits(state, 245, 313);
swap_bits(state, 246, 324);
swap_bits(state, 247, 335);
}

void cash128_non_linear(void *state) {
    int sbox[16] = {
        0x0E, 0x09, 0x0F, 0x00, 0x0D, 0x04, 0x0A, 0x0B,
        0x01, 0x02, 0x08, 0x03, 0x07, 0x06, 0x0B, 0x05,
    };

    for (int i = 0; i < RATE + CAPACITY; i += 8) {
        char *current_num = (char *)(state + (i/8));
        *current_num = (sbox[(int)((*current_num & 0xF0) >> 4)] << 4) | sbox[(int)((*current_num & 0x0F))];
    }
}

void customShifts(void *state, void *resultLeft, void *resultRight) {
    uint64_t N = (RATE + CAPACITY);
    for (int i = 0; i < (N / 64); i++) {
        *(uint64_t *)(resultLeft + i*8) = (*(uint64_t *)(state + i*8)) << 1;
    }

    for (int i = 1; i < (N/64); i++) {
        *(char *)(resultLeft + (i - 1)*8) |= ((*(char *)(state + (i + 1)*8 - 1)) >> 7) & 1; 
    }

    for (int i = 0; i < (N / 64); i++) {
        *(uint64_t *)(resultRight + i*8) = (*(uint64_t *)(state + i*8)) >> 1;
    }

    *(char *)(resultRight + 7) &= 0b01111111; 
    for (int i = 1; i < (N/64); i++) {
        *(char *)(resultRight + (i+1)*8 - 1) &= 0b01111111; 
        *(char *)(resultRight + (i+1)*8 - 1 ) |= ((*(char *)(state + (i - 1)*8)) << 7); 
    }
}

void customXorAnd512(void *a, void *b, void *c, void *d) {
    *(uint64_t *)(a + 0*8) = (*(uint64_t *)(b + 0*8))  ^ ((*(uint64_t *)(a + 0*8))  & (*(uint64_t *)(d + 0*8)))  ^ (*(uint64_t *)(c + 0*8));
    *(uint64_t *)(a + 1*8) = (*(uint64_t *)(b + 1*8))  ^ ((*(uint64_t *)(a + 1*8))  & (*(uint64_t *)(d + 1*8)))  ^ (*(uint64_t *)(c + 1*8));
    *(uint64_t *)(a + 2*8) = (*(uint64_t *)(b + 2*8))  ^ ((*(uint64_t *)(a + 2*8))  & (*(uint64_t *)(d + 2*8)))  ^ (*(uint64_t *)(c + 2*8));
    *(uint64_t *)(a + 3*8) = (*(uint64_t *)(b + 3*8))  ^ ((*(uint64_t *)(a + 3*8))  & (*(uint64_t *)(d + 3*8)))  ^ (*(uint64_t *)(c + 3*8));
    *(uint64_t *)(a + 4*8) = (*(uint64_t *)(b + 4*8))  ^ ((*(uint64_t *)(a + 4*8))  & (*(uint64_t *)(d + 4*8)))  ^ (*(uint64_t *)(c + 4*8));
    *(uint64_t *)(a + 5*8) = (*(uint64_t *)(b + 5*8))  ^ ((*(uint64_t *)(a + 5*8))  & (*(uint64_t *)(d + 5*8)))  ^ (*(uint64_t *)(c + 5*8));
    *(uint64_t *)(a + 6*8) = (*(uint64_t *)(b + 6*8))  ^ ((*(uint64_t *)(a + 6*8))  & (*(uint64_t *)(d + 6*8)))  ^ (*(uint64_t *)(c + 6*8));
    *(uint64_t *)(a + 7*8) = (*(uint64_t *)(b + 7*8))  ^ ((*(uint64_t *)(a + 7*8))  & (*(uint64_t *)(d + 7*8)))  ^ (*(uint64_t *)(c + 7*8));
}

unsigned char reverseBitF(unsigned char a) {
    return (a * 0x0202020202ULL & 0x010884422010ULL) % 1023;
}

// unsigned char reverseAsm(unsigned char a) {
//     __asm__ __volatile__ (R"(
//         mov cx, 8       
//     daloop:                   
//         ror di          
//         adc ax, ax      
//         dec cx          
//         jnz short daloop  
//     ;)");
// }

void expand8To512(char a, void* dest) {
    unsigned char aBarStraight = a ^ (0b10000001);
    char aBarReverse = reverseBitF(aBarStraight);

    memset(dest, aBarStraight, (RATE+CAPACITY)/8);
    for (int i = 0; i < (RATE+CAPACITY)/8; i+=2) {
        // *(char *)(dest + i + 1) = aBarStraight;
        *(char *)(dest + i)     = aBarReverse;
    }
    // first bit should be d1 instead of d1Bar so complement the first bit
    *(char *)(dest + 7) ^= 0b10000000;
    
    // last bit should also be d1 instead of d1Bar so complement the last bit
    *(char *)(dest + (RATE + CAPACITY) / 8 - 8) ^= 1;
}

char kappaCAState = KAPPA;

void kappaCA() {
    char a = (kappaCAState << 1) & 0b11111110;
    char b = (kappaCAState >> 1) & 0b01111111;

    kappaCAState = a ^ (kappaCAState & KAPPA) ^ b;
}

void CA90150_512 (void *state, void *ruleNumber, void *result, int cycles) {
    // consists of n_r rounds
    void *leftShiftedState[(RATE + CAPACITY) / 8];
    void *rightShiftedState[(RATE + CAPACITY) / 8];

    for (int i = 0; i < (RATE + CAPACITY) / 64; i++) {
        *(uint64_t *)(state + i*8) = bswap_64(*(uint64_t *)(state + i*8));
    }

    for (int j = 0; j < cycles; j++) {
        customShifts(state, leftShiftedState, rightShiftedState);
        customXorAnd512(state, leftShiftedState, rightShiftedState, ruleNumber);
    }

    for (int i = 0; i < (RATE + CAPACITY) / 64; i++) {
        *(uint64_t *)(state + i*8) = bswap_64(*(uint64_t *)(state + i*8));
    }

}

void cash128_linear(void *state, int cycles) {
    void *final[(RATE + CAPACITY) / 8];
    void *ruleNumber[(RATE + CAPACITY) / 8];

    memset(ruleNumber, 0, (RATE + CAPACITY) / 8);
    expand8To512(kappaCAState, ruleNumber);
    CA90150_512(state, ruleNumber, final, cycles);
    kappaCA();
}

void modified_cash128(void *state) {
	cash128_linear(state, RATE/2);
	cash128_permute(state);
	cash128_non_linear(state);
	cash128_permute(state);
	cash128_linear(state, RATE/4);
}

