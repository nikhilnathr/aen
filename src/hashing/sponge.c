#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "constants.h"
#include "header.h"

// There is a lot of hardcoded 8 in this implementation
// this is because we have used char for changing the pointer
// for bitwise operations and sizeof(char) is 8
void XOR(void *state, void *stream, int offset);

void hash(void *stream, void *digest, int len, void (* internalFunc)(void *state)) {

    void *state[(RATE + CAPACITY)/8];
    memset(state, '\0', sizeof(state));

    len = pad(stream, len);
    
    absorb(state, stream, len, internalFunc);
    // squeeze(stream, state, digest, internalFunc);

}

void squeeze(void *stream, void *state, void *digest, int streamBitOffset, void (* internalFunc)(void *state)) {
    printf("Has to absorb %d bits to state before squeezing\n", streamBitOffset);
    int len = pad(stream, streamBitOffset);
    // printBinary(stream, 10240, "after padding");

    absorb(state, stream, len, internalFunc);

    int i;
    for (i = 0; i < HASH_SIZE/RATE; i++) {
        memcpy(digest + (i*RATE/8), state, RATE/8); 
        (* internalFunc)(state);
    }

    if (HASH_SIZE%RATE != 0) {
        memcpy(digest + (i*RATE/8), state, (RATE - (HASH_SIZE%RATE) / 8));
    }
}

int absorb(void *state, void *stream, int len, void (* internalFunc)(void *state)) {
    int currentStartPos;
    for (currentStartPos = 0; currentStartPos + RATE <= len; currentStartPos += RATE) {
        XOR(state, stream, currentStartPos);
        (* internalFunc)(state);
    }
    if (len != currentStartPos) {
        memmove(stream, stream + (currentStartPos / 8), (len - currentStartPos) / 8);
    }
    return len - currentStartPos;
}

// padding scheme is 10*1
// stream: input block 
// len: length in bits of input block
int pad(void *stream, int len) {
    // number of 0s in padding
    int paddingZerosLength = ((len + 2) % RATE != 0) * (RATE - ((len + 2) % (RATE))); 

    printf("Input length is %d bits, needs %d padding\n", len, paddingZerosLength + 2);
    memset(stream + (len / 8), 0, (RATE - len) / 8);

    // add first 1 in 10*1
    *(char *)(stream + (len / 8)) |= 1 << (8 - (len % 8) - 1);

    // bits in between are zero

    // add last 1 in 10*1
    *(char *)(stream + ((len + paddingZerosLength + 1) / 8)) |= 1 << (8 - ((1 + len + paddingZerosLength) % 8) - 1);

    return len + paddingZerosLength + 2;
}

// XOR for specific use for XORing stream and state
// TODO: try to remove for loops in XOR
void XOR(void *state, void *stream, int offset) {
    // printf("\n");
    // printBinary(state, RATE, "state");
    // printBinary(stream + (offset/8), RATE, "stream");
    // for (int i = 0; i < RATE / 8; i++) {
    //     *(char *)(state + i) ^= *(char *)(stream + (offset/8) + i);
    // }
    for (int i = 0; i < RATE / 64; i++) {
        *(uint64_t *)(state + i*8) ^= *(uint64_t *)(stream + (offset/8) + i*8);
    }
    if ((RATE % 64) >= 32) {
        *(uint32_t *)(state + (RATE / 64) * 8) ^= *(uint32_t *)(stream + (offset/8) + (RATE/64) * 8);
    }
    if ((RATE % 32) >= 16) {
        *(uint16_t *)(state + (RATE / 32) * 4) ^= *(uint16_t *)(stream + (offset/8) + (RATE/32) * 4);
    }
    if ((RATE % 16) >= 8) {
        *(uint8_t *)(state + (RATE / 16) * 2) ^= *(uint8_t *)(stream + (offset/8) + (RATE/16) * 2);
    }
}