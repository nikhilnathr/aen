#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "constants.h"
#include "header.h"
#include "../helpers/header.h"

#define CHUNK_SIZE 10240

long int takeInputAsFile(char* path, void *stream, void *state);
long int takeInputAsString(void *stream, void *state);

// input is byte aligned in most cases
// so input should be byte aligned for this algorithm
int main (int argc, char **argv) {
    void *stream[CHUNK_SIZE];
    void *state[(RATE + CAPACITY)/8];
    void *digest[HASH_SIZE/8];

    memset(state, '\0', sizeof(state));

    int streamBitOffset = 0;
    if (argc == 2) {
        streamBitOffset = takeInputAsFile(argv[1], stream, state);
    } else {
        streamBitOffset = takeInputAsString(stream, state);
    }

    memset(stream + (streamBitOffset / 8), 0, (RATE - (streamBitOffset/8)));
    squeeze(stream, state, digest, streamBitOffset, modified_cash128);
    // squeeze(stream, state, digest, streamBitOffset, modified_mam128);

    binToHex(digest, HASH_SIZE);

    return 0;
}


long int takeInputAsString(void *stream, void *state) {; 
    printf("Input message string (ascii): ");
    if (scanf("%s", (char *)stream) != 1) {
        printf("Failed to read input.\n");
        exit(1);
    }
    long int len = strlen((char *)stream);
    // return absorb(state, stream, len * 8 , modified_mam128);
    return absorb(state, stream, len * 8 , modified_cash128);
}

long int takeInputAsFile(char* path, void *stream, void *state) {
    printf("File path is: '%s'\n", path);
    
    FILE *fp;
    fp = fopen(path, "r");
    if (!fp) {
        printf("Target file not found");
        exit(-1);
    }

    long int chunk=0;
    int chunkSize = CHUNK_SIZE;
    int i = 0;
    size_t readnow;
    int streamBitOffset, streamBitOffsetNew = 0;

    do {
        streamBitOffset = streamBitOffsetNew;
        readnow = fread(((char *) stream)+(streamBitOffset/8), 1, chunkSize - (streamBitOffset/8), fp);
        if (readnow < 0) {
            printf("\nRead Unsuccessful\n");
            free(stream);
            fclose(fp);
            exit(1);
        }

        chunk = chunk + readnow;
        streamBitOffsetNew = absorb(state, stream, readnow * 8 + streamBitOffset, modified_cash128);
        // streamBitOffsetNew = absorb(state, stream, readnow * 8 + streamBitOffset, modified_mam128);
        i++;
    } while (readnow == chunkSize - (streamBitOffset/8));

    fclose(fp);
    printf("Read %ld bytes in %d times last read size in %ld\n\n", chunk, i, readnow);

    return streamBitOffsetNew;
}