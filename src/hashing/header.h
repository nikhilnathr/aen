void hash(void *stream, void *digest, int len, void (* internalFunc)(void *state));
int pad(void *stream, int len);
int absorb(void *state, void *stream, int len, void (* internalFunc)(void *state));
void squeeze(void *stream, void *state, void *digest, int streamBitOffset, void (* internalFunc)(void *state));

void modified_cash128(void *state);
void modified_mam128(void *state);
