#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printChar(char n) {
    char buff[8];
    int len = 8;
    for (int i = 0; i < len; i++) {
        buff[i] = '0';
    }
    for (int i = 0; i < len; i++) {
        if (n & 1)
            buff[i] = '1';
    
        n >>= 1;
    }

    for (int i = 0; i < len; i++) {
        printf("%c", buff[len-1-i]);
    }
    printf(" ");
}

void printBinary(char * n, int len, void *desc) {
    for (int i = 0; i < (int)(len/8.0); i++) {
        printChar(*(n+i));
    }
    printf("| LENGTH %d | %s\n", len, (char *)desc);
}

void binToHex(void* digest, int len) {
    // printBinary(digest, HASH_SIZE, "bin to hex");
    for (int i = 0; i < len; i += 4) {
        char cur = 0;
        if (i%8 == 0) {
            cur = ((*(char *)(digest + (i/8))) & 0xF0) >> 4;
        } else {
            cur = ((*(char *)(digest + (i/8))) & 0x0F);
        }
        printf("%X", cur);
    }
    printf("\n");
}

void *alloc(long int size) {
    void * block = malloc(size/8);
    if (block) {
        printf("Allocated %zu Bytes from %p to %p bytes\n", size/8, block, block + size/8);
        return block;
    } else {
        printf("Error in allocation.\n");
        exit(1);
    }
}