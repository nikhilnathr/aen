#!/usr/bin/python3
from subprocess import Popen, PIPE, STDOUT
import sys

a=0xae560fbc7ab20b82a0cdfe

if (len(sys.argv) != 3 and len(sys.argv) != 4):
    print("Usage: %s number binary_name [initial_increment]" % sys.argv[0])
    quit()
inc = 0
if (len(sys.argv) == 4):
    inc = int(sys.argv[3])

for i in range(int(sys.argv[1])):
    # print(hex(a))
    p = Popen([sys.argv[2]], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    out = p.communicate(input=bytes(hex(i+a+inc), 'utf-8'))
    print(out[0].splitlines()[-1].decode())
