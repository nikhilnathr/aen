#!/bin/bash
if [ $# -ne 2 ]; then
    echo "Usage: $0 [number] [message]"
    exit -1
fi

source ./aenrc

python_output_taker="./out.py"
date=$(date '+%s')
mkdir -p "$new_tester_path"

new_tester_path="$new_tester_path/$date"

sed -i '/modified_cash128/,$d' ./src/hashing/modified_cash128.c
func=$(echo $2 | sed -e 's/->/\n/g' | sed -e 's/^l(/cash128_linear(state, /g' -e 's/R/RATE/g' -e 's/C/CAPACITY/g' -e 's/nl/cash128_non_linear(state)/g' -e 's/^p.*/cash128_permute(state)/g' -e 's/$/;/g' -e 's/^/\t/g')
echo -e "void modified_cash128(void *state) {\n$func\n}\n" >> ./src/hashing/modified_cash128.c

echo "Copying tester folder ($new_tester_path)"
cp -r "$tester_path" "$new_tester_path"

echo "Writing reason"
echo "$2" > "$new_tester_path/message.txt"

echo "Making new binary"
command -v gcc && releaseMode="release-gcc"
command -v clang && releaseMode="release"
make $releaseMode

echo "Copying the binary"
cp ./bin/main "$new_tester_path/main"

start_time=$(date '+%s')
echo "Running program $1 times"

num_block=10
input_block=$(( $1 / $num_block ))
num_block=$(( $num_block - 1 ))

for i in $( seq 0 $num_block )
do
    python3 $python_output_taker $input_block $new_tester_path/main $(( $i * $input_block )) > $new_tester_path/aen-2.0-$i &
done

while [ $(wc -l $new_tester_path/aen-2.0*  | grep total | awk -F' ' '{print $1}') -lt $1 ]
do
    echo -n -e "\rLoading $(( $(wc -l $new_tester_path/aen-2.0*  | grep total | awk -F' ' '{print $1}') * 100 / $1 ))%..."
    sleep 5
done&

trap "pkill -P $$" SIGTERM exit

wait
cat $new_tester_path/aen-2.0-* > $new_tester_path/aen-2.0


end_time=$(date  '+%s')
total_time=$(( $end_time - $start_time )) 

echo "Converting hex to binary"
xxd -r -p $new_tester_path/aen-2.0 $new_tester_path/aen-2.0.bin

cd "$new_tester_path"

inp="0
$new_tester_path/aen-2.0.bin
1
0
128
1"
./assess $1 < <(echo "$inp")

echo "Sending notification"
failed=$(cat $new_tester_path/experiments/AlgorithmTesting/finalAnalysisReport.txt | grep '*' | awk '{print $11, $12, $13, $14, $15}' | sort | uniq -c)

curl -X POST -d"{\"message\":\"$2\",\"failed\":\"$failed\",\"time\":\"$date\",\"time_taken\":\"$total_time\",\"runs\":\"$1\"}" "$notification_url"
